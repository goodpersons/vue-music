import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

// 自动生成的 全局样式
// import './assets/main.css'
// 导入tailwindcss 样式
import './tailwind.css';

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
