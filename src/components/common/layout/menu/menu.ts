import { Planet, Music, VideoOne, Fm, Like, Computer, DownloadThree, PlayTwo } from '@icon-park/vue-next';
import { ref, watch } from "vue";
import { useRoute, useRouter } from "vue-router";
interface IMune {
  name: string
  icon: any
  key: string
}

interface IMunes {
  name: string
  menus: IMune[]
}

export function useMenu(){
  const menu: IMunes[] = [
    {
      name: '在线音乐',
      menus: [
        {
          name: '推荐',
          icon: Planet,
          key: 'discover'
        },
        {
          name: '音乐馆',
          icon: Music,
          key: 'music'
        },
        {
          name: '视频',
          icon: VideoOne,
          key: 'video'
        },
        {
          name: '电台',
          icon: Fm,
          key: 'fm'
        }
      ]
    },
    {
      name: '我的音乐',
      menus: [
        {
          name: '我喜欢',
          icon: Like,
          key: 'like'
        },
        {
          name: '本地歌曲',
          icon: Computer,
          key: 'loal'
        },
        {
          name: '下载歌曲',
          icon: DownloadThree,
          key: 'download'
        },
        {
          name: '最近播放',
          icon: PlayTwo,
          key: 'zuijin'
        }
      ]
    }
  ]

  const route = useRoute()
  const router = useRouter()
  let currentKey = ref(route.meta.menu)

  watch(
    ()=> route.meta.menu,
    (menu)=>{
      currentKey.value = menu
    }
  )

  const click = async (menu:IMune)=>{
    await router.push({name: menu.key})
  }


  return {
    menu,
    currentKey,
    click
  }
}
