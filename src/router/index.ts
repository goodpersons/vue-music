import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('../views/HomeView.vue'),
      redirect: {name: 'discover'},
      children: [
        {
          path: '/discover',
          name: 'discover',
          component: () => import('../views/discover/discover.vue'),
          meta: {
            menu: 'discover'
          }
        },
        {
          path: '/music',
          name: 'music',
          component: () => import('../views/music/music.vue'),
          meta: {
            menu: 'music'
          }
        },
        {
          path: '/video',
          name: 'video',
          component: () => import('../views/discover/discover.vue'),
          meta: {
            menu: 'video'
          }
        },
        {
          path: '/fm',
          name: 'fm',
          component: () => import('../views/discover/discover.vue'),
          meta: {
            menu: 'discover'
          }
        },
        {
          path: '/like',
          name: 'like',
          component: () => import('../views/discover/discover.vue'),
          meta: {
            menu: 'like'
          }
        }

      ]
    }
  ]
})

export default router
